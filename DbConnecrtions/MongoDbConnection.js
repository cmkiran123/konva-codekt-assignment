const MongoClient = require("mongodb").MongoClient;

module.exports = async function () {
    const url = "mongodb://localhost:27017/";

    let client = await MongoClient.connect(url, {useNewUrlParser: true});
    return await client.db("ImageDatabase");
};
