module.exports.insertImage = function (db, schema, res) {
    db.collection("imagedatabases").insertOne(schema, (err, result) => {
        if (err) return console.log(err);
        res.end('Done Uploading');
    });
};

module.exports.deleteImage = function (db) {
    db.collection("imagedatabases").deleteMany((err, result) => {
        if (err) console.log(err);
        else console.log("Data Deleted");
    });
};

module.exports.getImage = async function (db) {
    return new Promise(async (resolve, reject) => {
        await db.collection("imagedatabases").find({}).toArray(  (err, result) => {
            if (err) {
                throw (err);
            } else {
                 if(result[0].data)
                    resolve(result[0].data.toString("base64"));
            }
        });
    });
};