let fs = require("fs");
let mongoDbMethods = require("./DbConnecrtions/MongoDbMethods");
let dbConnection = require("./DbConnecrtions/MongoDbConnection");


let multer = require('multer');

const express = require('express');
const app = express();
const port = 3000;

app.use(express.static("./UI"));

let upload = multer({dest: 'uploads/'});

app.post('/upload', upload.single('Image'), async function (req, res) {
    let img = fs.readFileSync(req.file.path);
    let schema = {
        contentType: req.file.mimetype,
        data: img
    };

    let db = await dbConnection();

    mongoDbMethods.deleteImage(db);
    mongoDbMethods.insertImage(db, schema, res);

});

app.get('/photos', async (req, res) => {
    let db = await dbConnection();

    await res.send(await mongoDbMethods.getImage(db, res));
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));