function ImageController($scope, $http, imageService) {

    $scope.uploadFile = function () {
        imageService.uploadImage($scope.photo);
    };

    $scope.downloadFile = function () {
        imageService.downloadImage();
    }
}

var app = angular.module('konvaAssignment', []);

let require = [
    "$scope",
    "$http",
    "imageService",
    "KonvaDrawService",
    ImageController
];
app.controller('ImageController', require);

