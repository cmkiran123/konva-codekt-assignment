function drawImage() {
    function draw(result) {
        let width = window.innerWidth;
        let height = window.innerHeight;

        let stage = new Konva.Stage({
            container: 'container',
            width: width,
            height: height
        });

        let layer = new Konva.Layer();
        stage.add(layer);

        let imageObj = new Image();
        imageObj.onload = function () {
            let canvas = new Konva.Image({
                x: 100,
                y: 100,
                image: imageObj,
                width: imageObj.width,
                height: imageObj.height,
                draggable: true,
                keepRatio: true,
                stroke: 'gray',
                strokeWidth: 10
            });
            layer.add(canvas);

            layer.draw();

            let scaleBy = 1.1;
            stage.on('wheel', event => {
                event.evt.preventDefault();
                let oldScale = stage.scaleX();

                let mousePointTo = {
                    x: stage.getPointerPosition().x / oldScale - stage.x() / oldScale,
                    y: stage.getPointerPosition().y / oldScale - stage.y() / oldScale
                };

                let newScale =
                    event.evt.deltaY > 0 ? oldScale * scaleBy : oldScale / scaleBy;
                stage.scale({x: newScale, y: newScale});

                let newPos = {
                    x:
                        -(mousePointTo.x - stage.getPointerPosition().x / newScale) *
                        newScale,
                    y:
                        -(mousePointTo.y - stage.getPointerPosition().y / newScale) *
                        newScale
                };
                stage.position(newPos);
                layer.batchDraw();
            });
        };
        imageObj.src = "data:image/png;base64," + result.data;
    }
    return draw;
}

let required = [
    drawImage
];

app.factory('KonvaDrawService', required);