function imageService($http, KonvaDrawService) {

    function downloadImage() {
        $http({
            url: '/photos',
            method: "GET",
        }).then(function (res, err) {
            new KonvaDrawService(res);
        })
    }

    function uploadImage(image) {
        var imageData = new FormData();
        imageData.append('Image', image);

        $http({
            url: '/upload',
            method: "POST",
            data: imageData,
            headers: {'Content-Type': undefined}
        }).then(function (response) {
            console.log("Saved Image to db");
        }).catch(err => console.log(err));
    }
    
    return {
        uploadImage : uploadImage,
        downloadImage: downloadImage
    };
    
}

let requires = [
    "$http",
    "KonvaDrawService",
    imageService
];
app.factory('imageService', requires);